/* table.h - MeowMeow, a stream encoder/decoder */


#ifndef _TABLE_H
#define _TABLE_H

#define ENCODER_INIT { "wolf", "wolF", "woLf", "woLF", \
                       "wOlf", "wOlF", "wOLf", "wOLF", \
                       "Wolf", "WolF", "WoLf", "WoLF", \
                       "WOlf", "WOlF", "WOLf", "WOLF" }

#endif	/* _TABLE_H */
